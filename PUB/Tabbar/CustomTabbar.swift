
import UIKit
import PTCardTabBar

class PTTabBarViewController: PTCardTabBarController {

    override func viewDidLoad() {
        let vc1 =  storyboard?.instantiateViewController(identifier: "GroupViewController") as!
            GroupViewController
        let vc2 =  storyboard?.instantiateViewController(identifier: "mapVC") as!
            ViewController
        let vc3 =  storyboard?.instantiateViewController(identifier: "UserCrawlVC") as!
            UserCrawlVC
        
        
        let vc4 = storyboard?.instantiateViewController(identifier: "SettingsVC") as!
            SettingsVC
        
        vc1.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "Icon-Groups"), tag: 1)
        vc2.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "Icon-Map"), tag: 2)
        vc3.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "Icon-Beer-Crawls"), tag: 3)
        vc4.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "settings"), tag: 4)
        
        
        self.viewControllers = [vc1, vc2, vc3, vc4]
        self.tabBarBackgroundColor = #colorLiteral(red: 0.9333333333, green: 0.6156862745, blue: 0, alpha: 1)
        self.tintColor = #colorLiteral(red: 0.2140719891, green: 0.2507144213, blue: 0.2696322799, alpha: 1)
        
             
        super.viewDidLoad()
        self.selectedIndex = 1
    }
}
