
import UIKit
import SDWebImage
import Firebase

class SettingsVC: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    private let color = UIColor(red: 33/255.0,
                                green: 33/255.0,
                                blue: 33/255.0,
                                alpha: 1)
    enum SideMenuItem: String, CaseIterable {
        case admin = "Admin Portal"
        case signout = "SignOut"
    }
    private var menuItems: [SideMenuItem] = [SideMenuItem.admin,SideMenuItem.signout]

    override func viewDidLoad() {
        super.viewDidLoad()
        if ViewController.currentUser?.isNotAdmin ?? false{
            self.menuItems = menuItems.filter({$0 != SideMenuItem.admin})
        }
        tableView.backgroundColor = color
        view.backgroundColor = color
        tableView.register(UINib(nibName:"MenuHeader", bundle: nil), forCellReuseIdentifier: "MenuHeaderCell")
        tableView.register(UINib(nibName:"MenuCell", bundle: nil), forCellReuseIdentifier: "MenucellClass")
        tableView.register(UINib(nibName:"signOutCell", bundle: nil), forCellReuseIdentifier: "signOutClass")
        tableView.separatorColor = .clear
    }
}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count + 1
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuHeaderCell", for: indexPath) as! MenuHeaderCell
            cell.lblName?.text = (ViewController.currentUser?.fullName ?? "").capitalized
            cell.lblName?.textColor = .white
            cell.lblName?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            cell.lblFriends?.text = ViewController.currentUser?.email ?? ""
            cell.lblFriends?.textColor = .white
            cell.imag?.makeRound()
            if let url = URL(string: ViewController.currentUser?.avatar ?? ""){
                cell.imag.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imag.sd_setImage(with: url, placeholderImage: UIImage(named: "userImage"))
            }else{
                cell.imag.image = UIImage(named: "userImage")
            }
            cell.backgroundColor = color
            cell.contentView.backgroundColor = color
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenucellClass", for: indexPath) as! MenucellClass
            cell.lblName?.text = menuItems[indexPath.row - 1].rawValue
            cell.lblName?.textColor = .white
            cell.backgroundColor = color
            cell.contentView.backgroundColor = color
            let image = UIImage(named: "next.png")
            image?.withTintColor(.white)
            let checkmark = UIImageView(frame:CGRect(x:0, y:0, width:15, height:15))
            checkmark.image = image
            checkmark.tintColor = (menuItems[indexPath.row - 1] == SideMenuItem.signout) ?  .clear : .white
            cell.accessoryView = checkmark
            return cell
        }
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 200
        }
        
        return 60
    }

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row != 0{
            if ( indexPath.row == 1){
                let vc = storyboard?.instantiateViewController(identifier: "adminVC") as! adminViewController
                self.present(vc, animated: true, completion: nil)
            }else{
                signOut()
            }
        }
    }
    
    func signOut() {
          let firebaseAuth = Auth.auth()
          do {
            try firebaseAuth.signOut()
          } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
          }
          let Login = storyboard?.instantiateViewController(identifier: Constants.Storyboard.Login) as? Login
          view.window?.rootViewController = Login
          view.window?.makeKeyAndVisible()
      }
      
}
