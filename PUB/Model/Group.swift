
import Foundation

struct Group: Codable{
    var createdAt:TimeInterval?
    var createdBy:String?
    var id:String?
    var members:[String]?
    var membersNames:[String]?
    var name:String?
    var type:Int?
    var imageUrl:String?
    var isSelectable:Bool? = false
    
    private enum CodingKeys: String, CodingKey {
        case createdAt, createdBy, id, members, membersNames, name, type, imageUrl
    }
}
