

import Foundation

struct Crawl: Codable{
    var id:String?
    let name:String?
    let schduleDate:String?
    let bars:[Bar]?
    let memebers:[String]?
    let userimages:[String]?
    
    var getSchduleDate:Date{
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "dd MMM yyyy HH:mm:ss"
        return dateFormatter.date(from: schduleDate!)!
    }
}
