
import Foundation

class Bar: Codable {
    var id:String?
    var Address:String?
    var Name:String?
    var Phone:String?
    var School:String?
    var Zip:Double?
    var isSelected:Bool? = false
    var starttime:String?
    var endtime:String?
    var Latitude:Double?
    var Longitude:Double?
    
    var getStartDate:Date{
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "dd MMM yyyy HH:mm:ss"
        return dateFormatter.date(from: starttime!)!
    }
    
    var getEndDate:Date{
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "dd MMM yyyy HH:mm:ss"
        return dateFormatter.date(from: endtime!)!
    }
}

