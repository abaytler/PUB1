//
//  File.swift
//  PUB
//
//  Created by Syed on 13/03/2021.
//

import Foundation

struct PubUser: Codable {
    let dob:String?
    let fName:String?
    let lName:String?
    let email:String?
    let school:String?
    let uid:String?
    let admin:Bool?
    let friends:[Friend]?
    let groups:[String]?
    let fcmToken:String?
    let crawalId:String?
    let avatar:String?
    
    var fullName:String{
        return (fName ?? "")+" "+(lName ?? "")
    }
    
    var isNotAdmin:Bool{
        (self.admin ?? false) ? false : true
    }
    
    var isSelected:Bool? = false
    
    private enum CodingKeys: String, CodingKey {
        case dob, fName, lName, email, school, uid, admin, friends, groups, fcmToken, crawalId, avatar
    }
}

struct Friend: Codable{
    let name:String?
    let id:String?
    var isSelected:Bool? = false
    
    private enum CodingKeys: String, CodingKey {
        case name, id
    }
}

