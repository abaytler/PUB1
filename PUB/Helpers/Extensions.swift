//
//  Extensions.swift
//  PUB
//
//  Created by Mohsin on 13/03/2021.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    public func setNavigationBar(){
        
        let navColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        let color: UIColor = navColor
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.navigationController!.navigationBar.barTintColor = color
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "AvenirNext-Medium", size: 20)! , NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}

extension UIImageView{
    func makeRound(){
        layer.cornerRadius = frame.size.width / 2
        layer.borderWidth = 0
        layer.masksToBounds = false
        layer.borderColor = UIColor.white.cgColor
        clipsToBounds = true
    }
}

extension UIButton{
    func makeRound(){
        layer.cornerRadius = frame.size.width / 2
        layer.borderWidth = 1.0
        layer.masksToBounds = false
        layer.borderColor = UIColor.white.cgColor
        clipsToBounds = true
    }
}

extension UIViewController{
    
    func finishLoading(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func startLoading(_ message:String){
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = message
        loadingNotification.animationType = .zoom
    }
    
    func alertMessage(message:String,completionHandler:(()->())?) {
        let alert = UIAlertController(title:"", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            completionHandler?()
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
