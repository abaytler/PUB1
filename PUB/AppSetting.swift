
import Foundation
import UIKit

class AppSettings{
    
    static var sharedSettings = AppSettings()
   
    private init(){}
    
    var userPassword:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"UserPassword")
            UserDefaults.standard.synchronize()
        }
        get{
          return UserDefaults.standard.string(forKey: "UserPassword")
        }
    }
    
    var userName:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"userName")
            UserDefaults.standard.synchronize()
        }
        get{
          return UserDefaults.standard.string(forKey: "userName")
        }
    }
    
    var userLogo:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"userLogo")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "userLogo")
        }
    }
    
    var defaultVideoOption:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"defaultVideoOption")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "defaultVideoOption")
        }
    }
    
    var userEmail:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"UserEmail")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "UserEmail")
        }
    }
    
    var accountType:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"AccountType")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "AccountType")
        }
    }
    
    var authToken:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"userAuthToken")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "userAuthToken")
        }
    }
    
    var loginMethod:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"loginMethod")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "loginMethod")
        }
    }
    
    var isCrawlJoin:Bool?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"isCrawlJoin")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: "isCrawlJoin")
        }
    }
    
    var isLocationAccessd:Bool?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"location")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: "location")
        }
    }
    
    var fullName:String?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"fullName")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.string(forKey: "fullName")
        }
    }
    
    var isAutoLogin:Bool?{
        set{
            UserDefaults.standard.set(newValue ?? "",forKey:"isAutoLYPLogin")
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: "isAutoLYPLogin")
        }
    }
}

