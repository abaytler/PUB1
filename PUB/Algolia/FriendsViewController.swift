

import UIKit
import Firebase
import SideMenu
import UBottomSheet
import SDWebImage
//Main class for implementing Dark and light mode easy to use and reusable
class BaseController: UIViewController{
    
    var modeColor:UIColor = .black
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if self.traitCollection.userInterfaceStyle == .dark {
            modeColor = .white
        } else {
            modeColor = .black
        }
    }

    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        if newCollection.userInterfaceStyle == .dark {
            modeColor = .white
        } else {
            modeColor = .black
        }
    }
}

class FriendsViewController: BaseController,UISearchBarDelegate {

    @IBOutlet var tableView: UITableView!
    var friends: [PubUser] = []
   
    var userManager = UserManager()
    private var sideMenu: SideMenuNavigationController?
    private var groupVC: GroupViewController?
    private var friendsController:FriendsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Friends"
        tableView.reloadData()
        self.view.backgroundColor = (modeColor == .white) ? .black : .white
        userManager.getUserFriends { (friends) in
            self.friends = friends
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                //self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        tableView.reloadData()
        self.view.backgroundColor = (modeColor == .white) ? .black : .white
    }
    
    @IBAction func addTapped(){
        let searchController = storyboard?.instantiateViewController(identifier: "searchvc") as! SearchViewController
        searchController.modalPresentationStyle = .currentContext
        
        searchController.friends = friends.map({$0.uid ?? ""})
        self.present(UINavigationController(rootViewController: searchController), animated: true)
    }
    
    @IBAction func cancelTapped(){
        self.dismiss(animated: true, completion: nil)
    }
}

extension FriendsViewController: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return friends.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendViewCell", for: indexPath) as! FriendViewCell
        let user: PubUser
        user = friends[indexPath.row]
        //let button = UIButton(type: .close)
        let image = UIImage(named: "minusSign")
        image?.withTintColor(modeColor)
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.frame = CGRect(x: 20, y: 20, width: 20, height: 20)
        button.setImage(image, for: .normal)
        button.tintColor = modeColor
    
        button.tag = indexPath.row
        button.addTarget(self, action:#selector(self.buttonClicked(_ :)), for: .touchUpInside)
        cell.accessoryView = button
        cell.name.textColor = modeColor
        cell.setup(user)
        return cell
  }
    
    @objc func buttonClicked(_ sender: UIButton) {
        let tapedUserId = friends[sender.tag].uid ?? ""
        let userId = Auth.auth().currentUser?.uid ?? ""
        userManager.removeFriend(userId: userId, tapedUserId: tapedUserId)
        friends.remove(at: sender.tag)
        tableView.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}


class FriendViewCell: UITableViewCell{
    @IBOutlet weak var imag:UIImageView!
    @IBOutlet weak var name:UILabel!
    
    func setup(_ friend:PubUser){
        self.imag.makeRound()
        name.text = friend.fullName
        if let url = URL(string: friend.avatar ?? ""){
            imag.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imag.sd_setImage(with: url, placeholderImage: UIImage(named: "userImage"))
        }else{
            imag.image = UIImage(named: "userImage")
        }
    }
}
