//
//  SearchViewController.swift
//  PUB
//
//  Created by Syed on 13/03/2021.
//

import UIKit
import Firebase

class SearchViewController: BaseController {
    
    @IBOutlet var tableView: UITableView!
    
    var searcher:AlgoliaSearcher?
    var users: [PubUser] = []
    var friends: [String] = []
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = (modeColor == .white) ? .black : .white
        setUpNavBar()
        self.title = "Search"
        searcher = AlgoliaSearcher()
        searcher?.callback = {[weak self] users in
            self?.users = users.filter({ (user) -> Bool in
                ((user.uid ?? "") != (Auth.auth().currentUser?.uid ?? "")) && ((user.fName ?? "") != "")
            })
            for friend in (self?.friends ?? []){
                self?.users = self?.users.filter({($0.uid ?? "") != friend}) ?? []
            }
            self?.tableView.reloadData()
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        tableView.reloadData()
        self.view.backgroundColor = (modeColor == .white) ? .black : .white
    }

    func setUpNavBar() {
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.searchBar.delegate = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Search Users"
        search.searchBar.tintColor = modeColor
        navigationItem.searchController = search
        navigationItem.hidesSearchBarWhenScrolling = false
    }
}

extension SearchViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        searcher?.searchCollection(forText: text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return users.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let user: PubUser
        user = users[indexPath.row]
        cell.textLabel?.text = user.fullName
        let button = UIButton(type: .contactAdd)
        button.tag = indexPath.row
        button.tintColor = modeColor
        button.addTarget(self, action:#selector(self.buttonClicked(_ :)), for: .touchUpInside)
        cell.accessoryView = button
        return cell
  }
    
  @objc func buttonClicked(_ sender: UIButton) {
    let tapedUserId = users[sender.tag].uid ?? ""
    let tapedUserName = users[sender.tag].fullName
    let userId = Auth.auth().currentUser?.uid ?? ""
    let username = ViewController.currentUser?.fullName ?? ""
    
    db.collection("users")
        .document(userId).collection("friends").document(tapedUserId).setData(["id":tapedUserId, "name":tapedUserName], merge: true)
    db.collection("users").document(tapedUserId).collection("friends").document(userId).setData(["id":userId, "name":username], merge: true)
    self.dismiss(animated: true, completion: nil)
    self.dismiss(animated: true, completion: nil)    
  }
}

