
import Foundation
import InstantSearchClient

class AlgoliaSearcher {
    
    var users = [PubUser]()
    private var collectionIndex : Index!
    private let query = Query()
    var callback:(([PubUser])->())?
    
    init() {
        setupAlgoliaSearch()
    }
    
    private func setupAlgoliaSearch() {
        let searchClient = Client(appID: "G7XAW4SGV7", apiKey: "320bb1aa15c78fadab9a5c81fc148bdb")
        let indexName = "users"
        collectionIndex = searchClient.index(withName: indexName)
        query.hitsPerPage = 10
        
        // Limiting the attributes to be retrieved helps reduce response size and improve performance.
        query.attributesToRetrieve = ["fName", "lName", "uid", "uName"]
        query.attributesToHighlight = ["fName", "lName"]
    }
    
    func searchCollection(forText searchString : String) {
        query.query = searchString
        collectionIndex.search(query) { (content, error) in
            guard let content = content else {
                if let error = error {
                    print(error.localizedDescription)
                }
                return
            }
            guard let hits = content["hits"] as? [[String: AnyObject]] else { return }
            if let jsonData = try? JSONSerialization.data(withJSONObject: hits, options: .prettyPrinted){
                let usersData = try? JSONDecoder().decode([PubUser].self, from: jsonData)
                self.users = usersData ?? []
                self.callback?(self.users)
                return
            }
            
            self.users = []
            self.callback?(self.users)
        }
    }
}
