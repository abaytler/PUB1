//
//  signUpViewController.swift
//  PUB
//
//  Created by Andrew Baytler on 2/19/21.
//

import Foundation
import Firebase
import UIKit
import SCSDKLoginKit
import SCSDKBitmojiKit

class signUpViewController: UIViewController {
    //
    //IMPORTS ALL THE VARIABLES FROM THE STORYBOARD INTO CODE
    //
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //THE LINE BELOW MAKES SURE TO HIDE THE KEYBOARD ONCE THE USER IS DONE TYPING THEIR INFO IN.
        self.hideKeyboardWhenTappedAround()
    }
    
    func validateFields() -> String? {
        print("\n \n \nVALIDATION STARTED...\n \n")
        if emailField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || passwordField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            print("\nFIELDS WERE EMPTY\n \n")
            return "Please fill in all the fields."
        }
        else {
        //You can add more validation below here as well.. for example if password is secure 53:55 in video
        print("\nPASSED VALIDATION!\n \n")
        return nil
        }
    }
    func showError(_ message:String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    @IBAction func backButton1(_ sender: Any) {
        transitionToLogin()
    }
    @IBAction func didCreateAccount(_ sender: Any) {
        
        let error = validateFields()
        if error != nil {
            //If something is wrong with the fields, show the error message.
            print("ERROR. ERROR. ERROR.")
            showError(error!)
        }
        else {
            let email = emailField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                //check for errors
                if err != nil {
                    //There was an error creating the user
                    print("\n \nError creating the user\n \n")
                    self.showError("Error creating user")
                    self.transitionToLogin()
                }
                else {
                   
                    //User created successfully
                    print("\n \nWE ARE ALMOST THERE!!!\n \n")
                    let db = Firestore.firestore()
                    db.collection("users").document(result!.user.uid).setData(["email":email,  "uid":result!.user.uid]) { (Error) in
                        if error != nil {
                            //Show error message
                            print("User data couldn't be saved on the database side")
                            self.showError("User data couldn't be saved on the database side")
                        }
                    }
                    print("\n \n------------------------------\nAWESOME USER IS CREATED\n")

                   
                    self.transitionToOnBoard1()
                }
            }
        }
    }
    
    func transitionToOnBoard1() {
        if #available(iOS 13.0, *) {
            let onBoard1ViewController = storyboard?.instantiateViewController(identifier: Constants.Storyboard.onBoard1ViewController) as? onBoard1ViewController
            view.window?.rootViewController = onBoard1ViewController
            view.window?.makeKeyAndVisible()
        } else {
            print("DIDN'T GET TO ONBOARD1 SCREEN")
        }
    }
    
    func transitionToLogin() {
        if #available(iOS 13.0, *) {
            let Login = storyboard?.instantiateViewController(identifier: Constants.Storyboard.Login) as? Login
            view.window?.rootViewController = Login
            view.window?.makeKeyAndVisible()
        } else {
            print("DIDN'T GET TO LOGIN SCREEN")
        }
    }
    
}
