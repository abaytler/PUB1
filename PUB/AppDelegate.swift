//
//  AppDelegate.swift
//  PUB
//
//  Created by Andrew Baytler on 2/13/21.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import CoreLocation
import FirebaseMessaging

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        PushNotificationManager.instance.registerForPushNotifications()
        BackgroundLocationManager.instance.start()
//        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
//
//        }
        
        
        //you can commented this line in order to disable the persistence
        if(AppSettings.sharedSettings.isCrawlJoin ?? false){
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "EntranceVC") as! EntranceVC
            UIApplication.shared.windows.first?.rootViewController = vc
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
        return true
    }
   
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    
//    func application(_ app: UIApplication,
//                     open url: URL,
//                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        let handled = SCSDKLoginClient.application(app, open: url, options: options)
//        return handled
//    }
}
