//
//  adminViewController.swift
//  PUB
//
//  Created by Andrew Baytler on 3/17/21.
//

import Foundation
import Firebase
import UIKit


class adminViewController: UIViewController {
    
    @IBAction func goHome(_ sender: Any) {
        self.transitionToMap()
    }
    
    func transitionToMap() {
        if #available(iOS 13.0, *) {
            let ViewController = storyboard?.instantiateViewController(identifier: Constants.Storyboard.ViewController) as? ViewController
            view.window?.rootViewController = ViewController
            view.window?.makeKeyAndVisible()
        } else {
            print("DIDN'T GET TO MAP")
        }
    }
}
