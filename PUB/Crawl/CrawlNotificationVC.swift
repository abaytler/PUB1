
import UIKit
import UBottomSheet
import Firebase

class CrawlNotificationVC: BaseController {
    
    @IBOutlet weak var lblname:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lbltime:UILabel!
    
    @IBOutlet weak var lblDay:UILabel!
    @IBOutlet weak var lblHour:UILabel!
    @IBOutlet weak var lblMinute:UILabel!
    @IBOutlet weak var lblSecond:UILabel!
    
    var timeEnd : Date?
    var crawl:Crawl?
    var callback:(()->())?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let date = crawl?.getSchduleDate, let start = crawl?.bars?.first?.getStartDate, let end = crawl?.bars?.last?.getEndDate{
            timeEnd = date
            lblAddress.text = crawl?.bars?.first?.Address ?? "-"
            lblDate.text = date.simpleformatted
            lblname.text = crawl?.name ?? ""
            lbltime.text = "\(start.timeformatted) - \(end.timeformatted)"
        }
        updateView()
    }
    
    func updateView() {
      // Initialize the label
      setTimeLeft()
      // Start timer
      Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.setTimeLeft), userInfo: nil, repeats: true)
    }
    
    @objc func setTimeLeft() {
        let timeNow = Date()
        if timeEnd?.compare(timeNow) == ComparisonResult.orderedDescending {
            
            let interval = timeEnd?.timeIntervalSince(timeNow)
            
            let days =  (interval! / (60*60*24)).rounded(.down)
            
            let daysRemainder = interval?.truncatingRemainder(dividingBy: 60*60*24)
            
            let hours = (daysRemainder! / (60 * 60)).rounded(.down)
            
            let hoursRemainder = daysRemainder?.truncatingRemainder(dividingBy: 60 * 60).rounded(.down)
            
            let minites  = (hoursRemainder! / 60).rounded(.down)
            
            let minitesRemainder = hoursRemainder?.truncatingRemainder(dividingBy: 60).rounded(.down)
            
            let scondes = minitesRemainder?.truncatingRemainder(dividingBy: 60).rounded(.down)
            
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 2
            
            lblDay.text = formatter.string(from: NSNumber(value:days))
            lblHour.text = formatter.string(from: NSNumber(value:hours))
            lblMinute.text = formatter.string(from: NSNumber(value:minites))
            lblSecond.text = formatter.string(from: NSNumber(value:scondes!))
        }
    }
    
    @IBAction func joinTapped(_ sender:UIButton){
        startLoading("")
        var users = crawl?.memebers ?? []
        var images = crawl?.userimages ?? []
        
        if !users.contains(Auth.auth().currentUser?.uid ?? ""){
            users.append(Auth.auth().currentUser?.uid ?? "")
            images.append(ViewController.currentUser?.avatar ?? "")
        }
        UserManager().updateUser(crawlid: crawl?.id ?? "", members: users,images: images) {[weak self] response in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.dismiss(animated: true, completion: nil)
                self?.callback?()
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
    
    @IBAction func closeTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
