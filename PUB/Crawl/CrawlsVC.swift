//
//  CrawlsVC.swift
//  PUB
//
//  Created by Mohsin on 24/04/2021.
//

import UIKit

class CrawlsVC: BaseController {

    @IBOutlet weak var tableView:UITableView!
    var crawls:[Crawl] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getAllCrawls()
    }
    
    func getAllCrawls(){
        startLoading("")
        BarManager.shared.fetchAllCrawls{[weak self](crawls) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.crawls = crawls
                self?.tableView.reloadData()
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
    
    func sendNotification(crawl:Crawl){
        startLoading("")
        BarManager.shared.sendNotificationForCrawl(crawl: crawl){[weak self] in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.dismiss(animated: true, completion: nil)
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
}

extension CrawlsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return crawls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CrawlCell", for: indexPath)
         cell.textLabel?.text = crawls[indexPath.row].name ?? "-"
         cell.detailTextLabel?.text = "Send notification"
         cell.tintColor = UIColor.white
         return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sendNotification(crawl: crawls[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
