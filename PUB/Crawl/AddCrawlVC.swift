
import UIKit

class AddCrawlVC: BaseController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtSchduleDay:UITextField!
    var datePicker = UIDatePicker()
    
    var selectedBars = [Bar]()

    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
        self.txtSchduleDay.inputView = datePicker
        self.datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        tableView.reloadData()
    }
    
    @IBAction func addTapped(_ sender:UIButton){
        
        let crawl = Crawl(id: "", name: txtName.text ?? "", schduleDate: datePicker.date.formatted, bars: selectedBars, memebers: [], userimages: [])
        
        startLoading("")
        BarManager.shared.createCrawl(crawl: crawl){[weak self] in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.dismiss(animated: true, completion: nil)
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: {
                    self?.resetData()
                    self?.dismiss(animated: true, completion: nil)
                })
            }
        }

    }
    
    private func resetData(){
        txtName.text = nil
        txtSchduleDay.text = nil
        tableView.reloadData()
    }
    
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        txtSchduleDay.text = datePicker.date.simpleformatted
    }
}

extension AddCrawlVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedBars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BarCell", for: indexPath) as! BarCell
        cell.bar = selectedBars[indexPath.row]
        return cell
   }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 124
    }
}

class BarCell: UITableViewCell {
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var txtStartTime: UITextField!
   @IBOutlet weak var txtEndTime: UITextField!
   let startdatePicker = UIDatePicker()
   let enddatePicker = UIDatePicker()
    
    override func awakeFromNib() {
        self.txtStartTime.text = nil
        self.txtEndTime.text = nil
        
        startdatePicker.datePickerMode = .dateAndTime
        enddatePicker.datePickerMode = .dateAndTime
        
        startdatePicker.preferredDatePickerStyle = .wheels
        enddatePicker.preferredDatePickerStyle = .wheels
        
        self.txtStartTime.inputView = startdatePicker
        self.txtEndTime.inputView = enddatePicker
        
        self.startdatePicker.addTarget(self, action: #selector(handleStartDatePicker), for: .valueChanged)
        self.enddatePicker.addTarget(self, action: #selector(handleEndDatePicker), for: .valueChanged)
    }
    
    @objc func handleStartDatePicker(_ datePicker: UIDatePicker) {
        txtStartTime.text = datePicker.date.formatted
        bar?.starttime = datePicker.date.formatted
    }
    
    @objc func handleEndDatePicker(_ datePicker: UIDatePicker) {
        txtEndTime.text = datePicker.date.formatted
        bar?.endtime = datePicker.date.formatted
    }
   
    var bar: Bar?{
        didSet {
           titleLabel?.text = bar?.Name ?? ""
            if let start = bar?.starttime, let end = bar?.endtime{
                txtStartTime.text = start
                txtEndTime.text = end
            }else{
                txtStartTime.text = nil
                txtEndTime.text = nil
            }
        }
    }
}

extension Date {
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy HH:mm:ss"
        return formatter
    }()
    
    static let simpleformatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    var formatted: String {
        return Date.formatter.string(from: self)
    }
    
    var simpleformatted: String {
        return Date.simpleformatter.string(from: self)
    }
    
    static let timeformatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        return formatter
    }()
    
    var timeformatted: String {
        return Date.timeformatter.string(from: self)
    }
}

