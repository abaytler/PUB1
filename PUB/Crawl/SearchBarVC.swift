

import UIKit

class SearchBarVC: BaseController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var nextButton: UIButton?

    var bars:[Bar] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.allowsMultipleSelection = true
        searchBar.delegate = self
    }
    
    func searchBarByZip(){
        startLoading("")
        BarManager.shared.fetchBarByZipCode(Int(searchBar.text ?? "0") ?? 0) {[weak self](bars) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.bars = bars
                self?.tableView.reloadData()
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
    
    var selectedBars: [Bar] {
        return bars.filter { return $0.isSelected ?? false }
    }
    
    @IBAction func next(_ sender: Any) {
        if selectedBars.count >= 3{
            self.performSegue(withIdentifier: "moveaddcrawl", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationvc = segue.destination as! AddCrawlVC
        destinationvc.selectedBars = selectedBars
    }
}

extension SearchBarVC: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBarByZip()
        self.view.endEditing(true)

    }
}

extension SearchBarVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? CustomCell {
         cell.bar = bars[indexPath.row]
        if bars[indexPath.row].isSelected ?? false{
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
         } else {
            tableView.deselectRow(at: indexPath, animated: false)
         }
         return cell
      }
      return UITableViewCell()
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        bars[indexPath.row].isSelected = true
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        bars[indexPath.row].isSelected = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

class CustomCell: UITableViewCell {
   @IBOutlet weak var titleLabel: UILabel?
   
    var bar: Bar?{
        didSet {
           titleLabel?.text = bar?.Name ?? ""
        }
    }

   override func awakeFromNib() {
      super.awakeFromNib()
      selectionStyle = .none
   }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
       super.setSelected(selected, animated: animated)
       accessoryType = selected ? .checkmark : .none
    }
}
