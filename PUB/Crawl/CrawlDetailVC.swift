
import UIKit

class CrawlDetailVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblname:UILabel!
    @IBOutlet weak var crawlname:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lbltime:UILabel!
    
    @IBOutlet weak var lblDay:UILabel!
    @IBOutlet weak var lblHour:UILabel!
    @IBOutlet weak var lblMinute:UILabel!
    @IBOutlet weak var lblSecond:UILabel!
    
    var timeEnd : Date?
    var crawl:Crawl!

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        if let date = crawl?.getSchduleDate, let start = crawl?.bars?.first?.getStartDate, let end = crawl?.bars?.last?.getEndDate{
            timeEnd = date
            //lblAddress.text = crawl?.bars?.first?.Address ?? "-"
            lblDate.text = date.simpleformatted
            lblname.text = crawl?.name ?? ""
            lbltime.text = "\(start.timeformatted) - \(end.timeformatted)"
            crawlname.text = crawl?.name ?? ""
        }
        updateView()
    }
    
    func updateView() {
      // Initialize the label
      setTimeLeft()
      // Start timer
      Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.setTimeLeft), userInfo: nil, repeats: true)
    }
    
    @IBAction func backTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func setTimeLeft() {
        let timeNow = Date()
        if timeEnd?.compare(timeNow) == ComparisonResult.orderedDescending {
            
            let interval = timeEnd?.timeIntervalSince(timeNow)
            
            let days =  (interval! / (60*60*24)).rounded(.down)
            
            let daysRemainder = interval?.truncatingRemainder(dividingBy: 60*60*24)
            
            let hours = (daysRemainder! / (60 * 60)).rounded(.down)
            
            let hoursRemainder = daysRemainder?.truncatingRemainder(dividingBy: 60 * 60).rounded(.down)
            
            let minites  = (hoursRemainder! / 60).rounded(.down)
            
            let minitesRemainder = hoursRemainder?.truncatingRemainder(dividingBy: 60).rounded(.down)
            
            let scondes = minitesRemainder?.truncatingRemainder(dividingBy: 60).rounded(.down)
            
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 2
            
            lblDay.text = formatter.string(from: NSNumber(value:days))
            lblHour.text = formatter.string(from: NSNumber(value:hours))
            lblMinute.text = formatter.string(from: NSNumber(value:minites))
            lblSecond.text = formatter.string(from: NSNumber(value:scondes!))
        }
    }
    
    @IBAction func inviteTapped(_ sender:UIButton){
        let vc = storyboard?.instantiateViewController(identifier: "InviteVC") as! InviteVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.crawl = crawl
        self.present(vc, animated: true, completion: nil)
    }
}

extension CrawlDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return crawl?.bars?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SheetBarCell") as! SheetBarCell
        if let bar = crawl?.bars?[indexPath.row]{
            cell.setup(bar)
        }
        cell.lblButton.setTitle("\(indexPath.row + 1)", for: .normal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
