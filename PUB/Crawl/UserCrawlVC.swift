

import UIKit
import SDWebImage

class UserCrawlVC: UIViewController {
    
    @IBOutlet weak var userImg:UIImageView!
    @IBOutlet weak var crawlName:UILabel!
    @IBOutlet weak var crawlDate:UILabel!
    
    @IBOutlet weak var usersCollectionView:UICollectionView!
    @IBOutlet weak var crawalsCollectionView:UICollectionView!
    
    let inset: CGFloat = 0
    let minimumLineSpacing: CGFloat = 1
    let minimumInteritemSpacing: CGFloat = 5
    var crawl:Crawl?
    var users:[String] = []
    var images:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usersCollectionView.delegate = self
        usersCollectionView.dataSource = self
        crawalsCollectionView.delegate = self
        crawalsCollectionView.dataSource = self
        
        if let url = URL(string: ViewController.currentUser?.avatar ?? ""){
            self.userImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.userImg.sd_setImage(with: url, placeholderImage: UIImage(named: "userImage"))
        }else{
            self.userImg.image = UIImage(named: "userImage")
        }
        getActiveCrawl()
    }
    
    private func setup(){
       
        if let date = crawl?.getSchduleDate{
            crawlDate.text = date.simpleformatted
            crawlName.text = crawl?.name ?? ""
        }
    }
    
    private func getActiveCrawl(){
        if let user = ViewController.currentUser{
            if (user.crawalId ?? "") != ""{
                BarManager.shared.fetchActiveCrawl(user: ViewController.currentUser!) {[weak self](crawl) in
                    self?.finishLoading()
                    self?.crawl = crawl
                    self?.users = crawl?.memebers ?? []
                    self?.images = crawl?.userimages ?? []
                    self?.usersCollectionView.reloadData()
                    self?.setup()
                    
                } failure: {[weak self](error) in
                    DispatchQueue.main.async {
                        self?.finishLoading()
                        self?.alertMessage(message: error.message, completionHandler: nil)
                    }
                }
            }
        }
    }
    
    @IBAction func viewCrawal(_ sender:UIButton){
        if let user = ViewController.currentUser{
            if (user.crawalId ?? "") != ""{
                let vc = storyboard?.instantiateViewController(identifier: "CrawlDetailVC") as! CrawlDetailVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.crawl = crawl
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension UserCrawlVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
            
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == usersCollectionView{
            return users.count
        }else{
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == usersCollectionView{
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CrawalUser", for: indexPath as IndexPath) as! CrawalUser
              if let url = URL(string: images[indexPath.row]){
                  cell.image.sd_imageIndicator = SDWebImageActivityIndicator.gray
                  cell.image.sd_setImage(with: url, placeholderImage: UIImage(named: "userImage"))
              }else{
                  cell.image.image = UIImage(named: "userImage")
              }
              return cell
             
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreviousCrawl", for: indexPath as IndexPath) as! PreviousCrawl
            return cell
        }
    }
    
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension UserCrawlVC: UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return  minimumInteritemSpacing
    }

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        return CGSize(width: 50, height: 50)
//    }
}

class CrawalUser: UICollectionViewCell{
    @IBOutlet weak var image:UIImageView!
}

class PreviousCrawl:UICollectionViewCell{
    
}
