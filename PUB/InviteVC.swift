
import UIKit
import SDWebImage
import Firebase

class InviteVC: BaseController {
    
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var tableView:UITableView!
    var groups:[Group] = []
    var friends: [PubUser] = []
    var crawl:Crawl!

    override func viewDidLoad() {
        super.viewDidLoad()
        getGroups()
        getFriends()
    }
    
    private func getFriends(){
        startLoading("")
        UserManager().getUserFriends {[weak self](friends) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.friends = friends
                self?.tableView.reloadData()
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
    
    @IBAction func backTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func getGroups(){
        //startLoading("")
        GroupManager.shared.fetchGroupByUserID(Auth.auth().currentUser?.uid ?? "") {[weak self](groups) in
            //self?.finishLoading()
            DispatchQueue.main.async {
                self?.groups = groups
                self?.collectionView.reloadData()
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                //self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
    
    @IBAction func newGroup(_ sender:UIButton){
        let vc = storyboard?.instantiateViewController(identifier: "CreateGroupVC") as! CreateGroupVC
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func newFriend(_ sender:UIButton){
        let searchController = storyboard?.instantiateViewController(identifier: "searchvc") as! SearchViewController
        searchController.modalPresentationStyle = .currentContext
        searchController.friends = friends.map({$0.uid ?? ""})
        self.present(UINavigationController(rootViewController: searchController), animated: true)
    }
    
    @IBAction func sendInvites(_ sender:UIButton){
        var selectedfriends:[String] = []
       
        //Selected groups
        for group in groups.filter({($0.isSelectable ?? false) == true}){
            for friend in (group.members ?? []){
                selectedfriends.append(friend)
            }
        }
        
        //Selected Users
        for friend in friends.filter({($0.isSelected ?? false) == true}){
            selectedfriends.append(friend.uid ?? "0")
        }
        
        if selectedfriends.uniqued().count > 0{
            startLoading("")
            BarManager.shared.sendInviteNotificationForCrawl(crawl: crawl, users: selectedfriends.uniqued()) {[weak self] in
                DispatchQueue.main.async {
                    self?.finishLoading()
                    self?.moveNext()
                }

            } failure: {[weak self](error) in
                DispatchQueue.main.async {
                    self?.finishLoading()
                    self?.alertMessage(message: error.message, completionHandler: nil)
                }
            }
        }
    }
    
    private func moveNext(){
        let vc = storyboard?.instantiateViewController(identifier: "EntranceVC") as! EntranceVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.crawl = crawl
        self.present(vc, animated: true, completion: nil)
    }
}

extension InviteVC: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return friends.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendInviteCell", for: indexPath) as! FriendInviteCell
        cell.setup(friends[indexPath.row])
        cell.name.text = (friends[indexPath.row].fullName ).capitalized
        cell.name.textColor = modeColor
        return cell
  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        friends[indexPath.row].isSelected = !(friends[indexPath.row].isSelected ?? false)
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}


extension InviteVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
            
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.groups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupInviteCell", for: indexPath as IndexPath) as! GroupInviteCell
        cell.setup(groups[indexPath.row])
        cell.name.text = (groups[indexPath.row].name ?? "").capitalized
        cell.name.textColor = modeColor
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        groups[indexPath.row].isSelectable = !(groups[indexPath.row].isSelectable ?? false)
        collectionView.reloadItems(at: [indexPath])
    }
}

extension InviteVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let itemsPerRow:CGFloat = 3
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
}

class GroupInviteCell: UICollectionViewCell{
    @IBOutlet weak var imag:UIImageView!
    @IBOutlet weak var name:UILabel!
    
    func setup(_ group:Group){
        self.imag.layer.cornerRadius = 10
        self.imag.clipsToBounds = true
        if let url = URL(string: group.imageUrl ?? ""){
            self.imag.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imag.sd_setImage(with: url, placeholderImage: UIImage(named: "userImage"))
            self.imag.layer.borderWidth = (group.isSelectable ?? false) ? 2 : 0
            self.imag.layer.borderColor = (group.isSelectable ?? false) ? UIColor.blue.cgColor : UIColor.clear.cgColor

        }else{
            self.imag.image = UIImage(named: "userImage")
            self.imag.layer.borderWidth = (group.isSelectable ?? false) ? 2 : 0
            self.imag.layer.borderColor = (group.isSelectable ?? false) ? UIColor.blue.cgColor : UIColor.clear.cgColor
        }
    }
}


class FriendInviteCell: UITableViewCell{
    @IBOutlet weak var imag:UIImageView!
    @IBOutlet weak var name:UILabel!
    @IBOutlet weak var checkImg:UIImageView!
    @IBOutlet weak var outerBorder:UIView!
    
    func setup(_ friend:PubUser){
        self.imag.makeRound()
        if let url = URL(string: friend.avatar ?? ""){
            self.imag.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imag.sd_setImage(with: url, placeholderImage: UIImage(named: "userImage"))
        }else{
            self.imag.image = UIImage(named: "userImage")
        }
        self.outerBorder.layer.borderWidth = (friend.isSelected ?? false) ? 2 : 0
        self.outerBorder.layer.borderColor = (friend.isSelected ?? false) ? UIColor.blue.cgColor : UIColor.clear.cgColor
        let img = UIImage(named: "dry-clean.png")
        img?.withTintColor(.gray)
        self.checkImg.image = (friend.isSelected ?? false) ? UIImage(named: "check.png") : img
    }
}

extension Sequence where Element: Hashable {
    func uniqued() -> [Element] {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}
