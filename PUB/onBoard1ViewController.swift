//
//  onBoard1ViewController.swift
//  PUB
//
//  Created by Andrew Baytler on 2/19/21.
//

import Foundation
import UIKit
import SwiftUI
import Firebase
import SCSDKBitmojiKit
import SCSDKLoginKit

class onBoard1ViewController: UIViewController {
   
    
    private var datePicker : UIDatePicker?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.preferredDatePickerStyle = UIDatePickerStyle.wheels
        datePicker?.addTarget(self, action: #selector(onBoard1ViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        inputTextField.inputView = datePicker
        print(datePicker as Any)
        print("passed top")
        self.hideKeyboardWhenTappedAround()
        
    }

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var fNameField: UITextField!
    @IBOutlet weak var lNameField: UITextField!
    @IBOutlet weak var schoolField: UITextField!
    @IBOutlet var inputTextField: UITextField!
    
    @objc func dateChanged(datePicker: UIDatePicker) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        inputTextField.text = dateFormatter.string(from: datePicker.date)
        if let date = dateFormatter.date(from: inputTextField.text!){
            let age = Calendar.current.dateComponents([.year], from: date, to: Date()).year!
        
            if age >= 21 {
                inputTextField.text = dateFormatter.string(from: datePicker.date)
                showButton.alpha = 0

            } else {
                showButton.alpha = 1
                inputTextField.text = "Must be 21 or older"
                
            }
        }
        return inputTextField.text!
    }
    

    @IBOutlet weak var showButton: UIButton!

    
    @IBAction func finishButton(_ sender: Any) {
        let userId = Auth.auth().currentUser?.uid
        let user: String = userName.text!
        let firstName: String = fNameField.text!
        let lastName: String = lNameField.text!
        let dob: String = inputTextField.text!
        let school: String = schoolField.text!
    
        print("Current user ID is: " + userId!)
        startLoading("")
        let db = Firestore.firestore()
        SCSDKLoginClient.login(from: self) {[weak self](success : Bool, error : Error?) in
            DispatchQueue.main.async {
                if error == nil {
                    SCSDKBitmojiClient.fetchAvatarURL {[weak self](avatarURL, error) in
                       if error == nil{
                        db.collection("users").document(userId!).setData(["uName":user,"fName":firstName, "lName":lastName, "dob":dob, "school":school, "admin":false, "avatar":avatarURL ?? ""], merge: true)
                        
                        DispatchQueue.main.async {
                            self?.finishLoading()
                            self?.transitionToMap()
                        }
                            
                }else{
                    DispatchQueue.main.async {
                        self?.finishLoading()
                    }
                    print(error?.localizedDescription ?? "")
                }
              }
                }else{
                    print(error?.localizedDescription ?? "")
                    db.collection("users").document(userId!).setData(["uName":user,"fName":firstName, "lName":lastName, "dob":dob, "school":school, "admin":false, "avatar":""], merge: true)
                    
                    DispatchQueue.main.async {
                        self?.finishLoading()
                        self?.transitionToMap()
                    }
                }
            }
        }
    }
    
    func transitionToMap() {
        if #available(iOS 13.0, *) {
            //as (where wanna go)
            let ViewController = storyboard?.instantiateViewController(identifier: Constants.Storyboard.ViewController) as? ViewController
            view.window?.rootViewController = ViewController
            view.window?.makeKeyAndVisible()
        } else {
            print("DIDN'T GET TO MAP")
        }
    }
    func transitionTofriends() {
        if #available(iOS 13.0, *) {
            let FriendsViewController = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.FriendsViewController) as? FriendsViewController
            view.window?.rootViewController = FriendsViewController
            view.window?.makeKeyAndVisible()
        } else {
            print("DIDN'T GET TO Friends SCREEN")
        }
    }
    
}
