//
//  addBarViewController.swift
//  PUB
//
//  Created by Andrew Baytler on 3/28/21.
//

import Foundation
import Firebase
import UIKit

class addBarViewController: UIViewController {
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var zipField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var schoolField: UITextField!
    @IBOutlet weak var latitudeField: UITextField!
    @IBOutlet weak var longitudeField: UITextField!
    
    @IBAction func addBar(_ sender: Any) {
        //Add a new bar to firebase
        let name: String = nameField.text!
        let address: String = addressField.text!
        let zip: String = zipField.text!
        let phone: String = phoneField.text!
        let school: String = schoolField.text!
        let latitude: String = latitudeField.text!
        let longitude: String = longitudeField.text!
        
        let db = Firestore.firestore()
        db.collection("BarLocations").document(name).setData(["Name":name, "Address":address, "Zip":zip, "Phone":phone, "School":school, "Latitude":latitude, "Longitude":longitude]) { (Error) in
            if Error != nil {
                //Show error message
                print("Bar location couldn't be saved on the database side")
            }
        }
    }
    
}
