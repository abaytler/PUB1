
import Foundation
import Firebase
import CodableFirebase
import CoreLocation
import GeoFire

class BarManager{
    
    static let shared = BarManager()
    private init(){}
    let networkError = NetworkError(status: Constants.NetworkError.internet, message: Constants.NetworkError.internetError)
    let genaricError = NetworkError(status: Constants.NetworkError.generic, message: Constants.NetworkError.genericError)
    
    let db = Firestore.firestore()
    var storageRef = Storage.storage().reference()
    
    
    func fetchBarByZipCode(_ zipcode:Int, completion: @escaping  ([Bar])->(),failure: @escaping (_ error: NetworkError) -> Void) {
        var bars:[Bar] = []
        if Network.isAvailable{
            let docRef = Firestore.firestore().collection("BarLocations").whereField("Zip", isEqualTo: zipcode)
            docRef.getDocuments { (querySnapshot, err) in
                if let err = err {
                    print(err.localizedDescription)
                    completion([])
                    return
                }else if querySnapshot!.documents.count > 0 {
                    bars.removeAll()
                    querySnapshot?.documents.forEach({ (doc) in
                        let dataDescription = doc.data()
                        if let jsonData = try? JSONSerialization.data(withJSONObject: dataDescription, options: .prettyPrinted){
                            let decoder = JSONDecoder()
                            //decoder.dateDecodingStrategy = .iso8601
                            let bar = try? decoder.decode(Bar.self, from: jsonData)
                            bars.append(bar!)
                        }
                    })
                    completion(bars)
                }else{
                    completion([])
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func fetchActiveCrawl(user:PubUser,completion: @escaping  (Crawl?)->(),failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            let docRef = Firestore.firestore().collection("barCrawl").document(user.crawalId ?? "")
            docRef.getDocument { (querySnapshot, err) in
                if let err = err {
                    print(err.localizedDescription)
                    completion(nil)
                    return
                }
               
                if  let dataDescription = querySnapshot?.data(), let jsonData = try? JSONSerialization.data(withJSONObject: dataDescription, options: .prettyPrinted){
                    let decoder = JSONDecoder()
                    let crawl = try? decoder.decode(Crawl.self, from: jsonData)
                    completion(crawl)
                }else{
                    completion(nil)
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func fetchAllCrawls(completion: @escaping  ([Crawl])->(),failure: @escaping (_ error: NetworkError) -> Void) {
        var crawls:[Crawl] = []
        if Network.isAvailable{
            let docRef = Firestore.firestore().collection("barCrawl")
            docRef.getDocuments { (querySnapshot, err) in
                if let err = err {
                    print(err.localizedDescription)
                    completion([])
                    return
                }else if querySnapshot!.documents.count > 0 {
                    crawls.removeAll()
                    querySnapshot?.documents.forEach({ (doc) in
                        let dataDescription = doc.data()
                        if let jsonData = try? JSONSerialization.data(withJSONObject: dataDescription, options: .prettyPrinted){
                            let decoder = JSONDecoder()
                            //decoder.dateDecodingStrategy = .iso8601
                            let crawl = try? decoder.decode(Crawl.self, from: jsonData)
                            crawls.append(crawl!)
                        }
                    })
                    completion(crawls)
                }else{
                    completion([])
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func sendInviteNotificationForCrawl(crawl:Crawl,users:[String],_ completion: @escaping () -> Void,failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            let encoder = FirebaseEncoder()
            encoder.dateEncodingStrategy = .iso8601
            guard let data = try? encoder.encode(crawl) else{
                failure(genaricError)
                return
            }
            
            self.getUserDeviceToken(ids: users, {[weak self](tokens) in
                if tokens.isEmpty{
                    failure(NetworkError(status: Constants.NetworkError.generic, message:"No users Available"))
                    return
                }
                self?.sendPushNotification(crawl: data as! [String : Any], tokens: tokens, { (response) in
                    if response == true{
                        completion()
                    }else{
                        failure(NetworkError(status: Constants.NetworkError.generic, message:"Error sending push notification"))
                    }
                }, failure: {(error) in
                    failure(NetworkError(status: Constants.NetworkError.generic, message:"Error sending push notification"))
                })
            })

        }else{
            failure(networkError)
        }
    }
    
    func sendNotificationForCrawl(crawl:Crawl,_ completion: @escaping () -> Void,failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            let encoder = FirebaseEncoder()
            encoder.dateEncodingStrategy = .iso8601
            guard let data = try? encoder.encode(crawl) else{
                failure(genaricError)
                return
            }
            
            self.searchUsersNearCrawl(bar: crawl.bars?.first) {[weak self](users) in
                if users.isEmpty{
                    failure(NetworkError(status: Constants.NetworkError.generic, message:"No User found"))
                    return
                }

                self?.getUserDeviceToken(ids: users, {[weak self](tokens) in
                    if tokens.isEmpty{
                        failure(NetworkError(status: Constants.NetworkError.generic, message:"No token found"))
                        return
                    }
                    self?.sendPushNotification(crawl: data as! [String : Any], tokens: tokens, { (response) in
                        if response == true{
                            completion()
                        }else{
                            failure(NetworkError(status: Constants.NetworkError.generic, message:"Error sending push notification"))
                        }
                    }, failure: {(error) in
                        failure(NetworkError(status: Constants.NetworkError.generic, message:"Error sending push notification"))
                    })
                })

            } failure: {(error) in
                failure(NetworkError(status: Constants.NetworkError.generic, message:"Error"))
            }
        }else{
            failure(networkError)
        }
    }

    
    func createCrawl(crawl:Crawl,_ completion: @escaping () -> Void,failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            let groupRef = Firestore.firestore().collection("barCrawl").document()
            var usercrawl = crawl
            usercrawl.id = groupRef.documentID
            let encoder = FirebaseEncoder()
            encoder.dateEncodingStrategy = .iso8601
            guard let data = try? encoder.encode(usercrawl) else{
                failure(genaricError)
                return
            }
            
            groupRef.setData(data as! [String : Any]) {(error) in
                if error != nil{
                    failure(NetworkError(status: Constants.NetworkError.generic, message:"Error Creating New Bar Crawl with failure"))
                }else{
                    completion()
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func getUserDeviceToken(ids:[String],_ completion: @escaping ([String])->()) {
        var tokens:[String] = []
        for id in ids{
            UserManager().getUser(id: id){(user) in
                if let user = user{
                    if user.crawalId == nil || user.crawalId == ""{
                        tokens.append(user.fcmToken ?? "token")
                    }else{
                        tokens.append("")
                    }
                    
                    if tokens.count == ids.count{
                        completion(tokens.filter({$0 != ""}))
                    }
                }
            } 
        }
    }
    
    func searchUsersNearCrawl(bar:Bar?,_ completion: @escaping ([String]) -> Void,failure: @escaping (_ error: NetworkError) -> Void){
        var users:[String] = []
        if Network.isAvailable{
            users.removeAll()
            let geoFirestoreRef = Firestore.firestore().collection("Geolocations")
            let geoFirestore = GeoFirestore(collectionRef: geoFirestoreRef)
            
            let circleQuery = geoFirestore.query(withCenter: CLLocation(latitude: bar?.Latitude ?? 0.0, longitude: bar?.Longitude ?? 0.0), radius: 50.0)
            
//            let circleQuery = geoFirestore.query(withCenter: CLLocation(latitude: 31.449623093250157, longitude: 74.43681875952169), radius: 50.0)
                                
            _ = circleQuery.observe(.documentEntered, with: { (key, location) in
                print("The document with documentID '\(key ?? "-")' entered the search area and is at location '\(location)'")
                users.append(key ?? "")
            })
            
            _ = circleQuery.observeReady {
                print("All initial data has been loaded and events have been fired!")
                circleQuery.removeAllObservers()
                completion(users)
            }
 
        }else{
            failure(networkError)
        }
    }
    
    func sendPushNotification(crawl: [String : Any], tokens:[String],_ completion: @escaping (Bool) -> Void,failure: @escaping (_ error: NetworkError) -> Void) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["registration_ids" : tokens,
                                           "notification" : ["title" : "New Crawl", "body" : "", "sound": "default"],
                                           "data" : crawl,
                                           "priority": "high"]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAA0Qm4W6s:APA91bGGt5gGM_wSX0k9tswBmiVMEVpAvownHXoDXCNQluTWLRJXyD3bQrX6wU6kRT7xj_LJZFzlTcdZFw4ELpIJL7l1HDgSW7MXJQ4IYrG6hDsGN-animM3M5EpizaKUypHxm5Y3LuA", forHTTPHeaderField: "Authorization")
        if Network.isAvailable{
            let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
                do {
                    if let jsonData = data {
                        if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                            NSLog("Received data:\n\(jsonDataDict))")
                            completion(true)
                        }
                    }
                } catch let err as NSError {
                    completion(false)
                    print(err.debugDescription)
                }
            }
            task.resume()
        }else{
            failure(networkError)
        }
    }
}


//            self.sendPushNotification(crawl: data as! [String : Any], tokens: ["d8NnPLMD0kkjlcGGYs-Lqq:APA91bHaStVrj4DWxxrdlqbnEyFKzZMI__6R1Q1MMOnCf8Kcl9JJEZt3LWnrSfTs6UxTIFW4nD-892hVixQpt44xtrUHJLyOFKCwHCs9U8NV54fzR-3Si57f4DjPs4Rkj_QjVm-fvAoh"], { (response) in
//                if response == true{
//                    completion(true)
//                }else{
//                    completion(false)
//                }
//            }, failure: {(error) in
//                completion(false)
//            })
//
