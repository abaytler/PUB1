
import Foundation
import Firebase

class UserManager{
    
    let db = Firestore.firestore()
    let networkError = NetworkError(status: Constants.NetworkError.internet, message: Constants.NetworkError.internetError)
    
    func updateUser(crawlid:String,members:[String],images:[String],_ completion: @escaping (Bool)->(),failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            Firestore.firestore().collection("users").document(Auth.auth().currentUser?.uid ?? "").setData(["crawalId":crawlid,"members":members], merge: true) {(error) in
                if error == nil{
                  
                    completion(true)
                }
                completion(false)
            }
        }else{
            failure(networkError)
        }
    }
    
    
    
    func getUser(id:String,_ completion: @escaping (PubUser?)->()) {
        if Network.isAvailable{
            let docRef = Firestore.firestore().collection("users").whereField("uid", isEqualTo: id)
            docRef.getDocuments { (querySnapshot, err) in
                if let err = err {
                    print(err.localizedDescription)
                    completion(nil)
                    return
                } else if querySnapshot!.documents.count != 1 {
                    completion(nil)
                    print("More than one document or none")
                } else {
                    if let document = querySnapshot?.documents.first{
                        let dataDescription = document.data()
                        if let jsonData = try? JSONSerialization.data(withJSONObject: dataDescription, options: .prettyPrinted){
                            let user = try? JSONDecoder().decode(PubUser.self, from: jsonData)
                            completion(user)
                        }
                    }
                }
            }
        }else{
            completion(nil)
        }
    }
    
    func getUserFriends(_ completion: @escaping ([PubUser])->(),failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            var friends:[Friend] = []
            var users:[PubUser] = []
            let docRef = Firestore.firestore().collection("users").document(Auth.auth().currentUser?.uid ?? "").collection("friends")
            docRef.addSnapshotListener { (querySnapshot, err) in
                if let err = err {
                    print(err.localizedDescription)
                    completion([])
                    return
                } else if querySnapshot!.documents.count > 0 {
                    friends.removeAll()
                    querySnapshot?.documents.forEach({ (doc) in
                        let dataDescription = doc.data()
                        if let jsonData = try? JSONSerialization.data(withJSONObject: dataDescription, options: .prettyPrinted){
                            let friend = try? JSONDecoder().decode(Friend.self, from: jsonData)
                            friends.append(friend!)
                        }
                    })
                    if friends.count == 0{
                        completion([])
                    }
                    var i = 0
                    users.removeAll()
                    for friend in friends{
                        UserManager().getUser(id: friend.id ?? "") {(mainuser) in
                            DispatchQueue.main.async {
                                if let user = mainuser{
                                    users.append(user)
                                }
                                i+=1
                                if i == friends.count{
                                    completion(users)
                                }
                            }
                        }
                    }
                    
                }else{
                    completion([])
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func removeFriend(userId:String, tapedUserId:String){
        
        let currentUserQuery = db.collection("users").document(userId).collection("friends").whereField("id", isEqualTo: tapedUserId)

        currentUserQuery.getDocuments(completion: { (snapshot, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                for document in snapshot!.documents {
                    self.db.collection("users").document(userId).collection("friends").document("\(document.documentID)").delete()
            }
        }})

        let friendUserQuery = db.collection("users").document(tapedUserId).collection("friends").whereField("id", isEqualTo: userId)

        friendUserQuery.getDocuments(completion: { (snapshot, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                for document in snapshot!.documents {
                    self.db.collection("users").document(tapedUserId).collection("friends").document("\(document.documentID)").delete()
            }
        }})
    }
}
