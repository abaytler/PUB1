
import Foundation
import Firebase
import CodableFirebase

class GroupManager{
    
    static let shared = GroupManager()
    private init(){}
    let networkError = NetworkError(status: Constants.NetworkError.internet, message: Constants.NetworkError.internetError)
    let genaricError = NetworkError(status: Constants.NetworkError.generic, message: Constants.NetworkError.genericError)
    
    let db = Firestore.firestore()
    var storageRef = Storage.storage().reference()
    
    func uploadLogo(id:String,image: UIImage, completion: @escaping (URL?) -> Void,failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            let imageData = image.jpegData(compressionQuality: 0.8)!
            let uuid = UUID().uuidString
            let imageRef = storageRef.child("GroupImages").child(id).child("\(uuid).jpg")
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
        
            imageRef.putData(imageData, metadata: metadata) { (metadata, error) in
                if error != nil {
                    completion(nil)
                } else {
                    imageRef.downloadURL { url, error in
                        Firestore.firestore().collection("groups").document(id).setData(["imageUrl":url?.absoluteURL.absoluteString ?? ""], merge: true)
                        completion(url)
                    }
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func createGroup(group:Group, completion: @escaping (Bool?,String) -> Void,failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            let groupRef = Firestore.firestore().collection("groups").document()
            var userGroup = group
            userGroup.id = groupRef.documentID
            
            
            guard let data = try? FirebaseEncoder().encode(userGroup) else{
                failure(genaricError)
                return
            }
            groupRef.setData(data as! [String : Any]) {(error) in
                if let _ = error{
                    failure(NetworkError(status: Constants.NetworkError.generic, message:"Error Creating Group"))
                }else{
                    completion(true,groupRef.documentID)
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func updateGroup(group:Group, completion: @escaping (Bool?,String) -> Void,failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            let groupRef = Firestore.firestore().collection("groups").document(group.id ?? "")
            guard let data = try? FirebaseEncoder().encode(group) else{
                failure(genaricError)
                return
            }
            groupRef.updateData(data as! [String : Any]) {(error) in
                if let _ = error{
                    failure(NetworkError(status: Constants.NetworkError.generic, message:"Error Creating Group"))
                }else{
                    completion(true,groupRef.documentID)
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func fetchGroupByUserID(_ id:String, completion: @escaping  ([Group])->(),failure: @escaping (_ error: NetworkError) -> Void) {
        var groups:[Group] = []
        if Network.isAvailable{
            let docRef = Firestore.firestore().collection("groups").whereField("members", arrayContains: id)
            docRef.addSnapshotListener { (querySnapshot, err) in
                if let _ = err {
                    completion([])
                    return
                } else if querySnapshot!.documents.count > 0 {
                    groups.removeAll()
                    querySnapshot?.documents.forEach({ (doc) in
                        let dataDescription = doc.data()
                        if let jsonData = try? JSONSerialization.data(withJSONObject: dataDescription, options: .prettyPrinted){
                            let group = try? JSONDecoder().decode(Group.self, from: jsonData)
                            groups.append(group!)
                        }
                    })
                    completion(groups)
                }
            }
        }else{
            failure(networkError)
        }
    }
    
    func deleteUserFromGroup(_ id:String,members:[String],membersNames:[String],completion: @escaping  ()->(),failure: @escaping (_ error: NetworkError) -> Void)  {
        if Network.isAvailable{
            Firestore.firestore().collection("groups").document(id).setData(["members":members,"membersNames":membersNames], merge: true) { (error) in
                if error == nil{
                    completion()
                }else{
                    failure(NetworkError(status: Constants.NetworkError.generic, message:"Error Deleting User"))
                }
            }
            
        }else{
            failure(networkError)
        }
    }
    
    func addUserInGroup(_ id:String,members:[String],membersNames:[String],completion: @escaping  ()->(),failure: @escaping (_ error: NetworkError) -> Void)  {
        if Network.isAvailable{
          Firestore.firestore().collection("groups").document(id).setData(["members":members,"membersNames":membersNames], merge: true){ (error) in
            if error == nil{
                completion()
            }else{
                failure(NetworkError(status: Constants.NetworkError.generic, message:"Error Adding User"))
            }
          }
        }else{
            failure(networkError)
        }
    }
    
    func deleteGroupByUserID(_ id:String, completion: @escaping  (String)->(),failure: @escaping (_ error: NetworkError) -> Void) {
        if Network.isAvailable{
            Firestore.firestore().collection("groups").document(id).delete {[weak self](error) in
                if error == nil {
                    self?.storageRef.child("GroupImages").child(id).delete(completion: nil)
                    completion("Group Deleted Succssfully")
                }else{
                    failure(NetworkError(status: Constants.NetworkError.generic, message:"Error Deleting Group"))
                }
            }
        }else{
            failure(networkError)
        }
    }
}



