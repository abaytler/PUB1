//
//  Login.swift
//  PUB
//
//  Created by Andrew Baytler on 2/14/21.
//

import Foundation
import UIKit
import Firebase
import AVKit

//
//THIS EXTENSION CAN BE USED IN ANY VIEW CONTROLLER FILE AND CAN BE CALLED BY DOING: self.hideKeyboardWhenTappedAround() INSIDE THE VIEWDIDLOAD() FUNCTION
//
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

//
//MAIN CODE STARTS HERE
//

class Login: UIViewController {
    //
    //IMPORTS ALL THE VARIABLES FROM THE STORYBOARD INTO CODE
    //
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    var videoPlayer:AVPlayer?
    var videoPlayerLayer:AVPlayerLayer?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //THE LINE BELOW MAKES SURE TO HIDE THE KEYBOARD ONCE THE USER IS DONE TYPING THEIR INFO IN.
        print("📗 Login screen is shown!")
        self.hideKeyboardWhenTappedAround()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        //CALLS THE FUNCTION TO SETUP THE VIDEO IN THE BACKGROUND
        setUpView()
    }
    
    //
    //SETS UP THE DJ VIDEO IN THE BACKGROUND TO A LOOP
    //
    func setUpView() {
        let bundlePath = Bundle.main.path(forResource: "djvid1", ofType: "mp4")
        guard bundlePath != nil else {
            return
        }
        let url = URL(fileURLWithPath: bundlePath!)
        let item = AVPlayerItem(url: url)
        videoPlayer = AVPlayer(playerItem: item)
        videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
        videoPlayerLayer?.frame = CGRect(x: -self.view.frame.size.width*1.5, y: 0, width: self.view.frame.size.width*4, height: self.view.frame.size.height)
        
        view.layer.insertSublayer(videoPlayerLayer!, at: 0)
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer?.currentItem, queue: .main) { [weak self] _ in
            self?.videoPlayer?.seek(to: CMTime.zero)
            self?.videoPlayer?.play()
        }
        videoPlayer?.playImmediately(atRate: 1)
    }
    
    //
    //LOG THE USER IN AND TRANSITION THEM TO HOME PAGE/MAP PAGE.
    //"e.k.feeney9@gmail.com"//
    //"feeney99"
    @IBAction func loginButton(_ sender: UIButton) {
        
        let email = usernameField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //Sign in the user
        Auth.auth().signIn(withEmail: email, password: password) {
            (result, error) in
            if error != nil {
                //Couldn't sign in
                self.errorLabel.text = error!.localizedDescription
                self.errorLabel.alpha = 1
            }
            else {
                print("\n \nUser logged in successfully!\n")
                let userId = Auth.auth().currentUser?.uid
                let email = Auth.auth().currentUser?.email
                print("Current user ID is: " + userId!)
                print("Current user Email is: " + email!)
                print("\nTransitoning to map...\n \n")
                self.transitionToMap()
            }
        }
    }

    //
    //CODE FOR TRANSITIONING THE STORYBOARD TO MAP
    //
    func transitionToMap() {
        if #available(iOS 13.0, *) {
            
            let ViewController = storyboard?.instantiateViewController(identifier: "PTTabBarViewController") as? UITabBarController
            view.window?.rootViewController = ViewController
            view.window?.makeKeyAndVisible()

//            let ViewController = storyboard?.instantiateViewController(identifier: Constants.Storyboard.ViewController) as? ViewController
//            view.window?.rootViewController = ViewController
//            view.window?.makeKeyAndVisible()
        } else {
            print("DIDN'T GET TO MAP")
        }
    }
    
    @IBAction func signUpButton(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let signUpViewController = storyboard?.instantiateViewController(identifier: Constants.Storyboard.signUpViewController) as? signUpViewController
            view.window?.rootViewController = signUpViewController
            view.window?.makeKeyAndVisible()
        } else {
            print("DIDN'T GET TO SIGN UP")
        }
        
    }
    
}
