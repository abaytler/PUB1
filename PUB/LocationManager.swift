import Foundation
import CoreLocation
import UIKit
import Firebase
import SCSDKBitmojiKit

class BackgroundLocationManager :NSObject, CLLocationManagerDelegate {

    static let instance = BackgroundLocationManager()
    static let BACKGROUND_TIMER = 18000.0 // restart location manager every 5 hours

    let locationManager = CLLocationManager()
    var timer:Timer?
    var currentBgTaskId : UIBackgroundTaskIdentifier?
    var lastLocationDate : Date = Date()

    private override init(){
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.activityType = .other;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.allowsBackgroundLocationUpdates = true

        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }

    @objc func applicationEnterBackground(){
        print("applicationEnterBackground")
        if(timer == nil){
            start()
        }
    }

    func start(){
        if((locationManager.authorizationStatus == CLAuthorizationStatus.authorizedAlways) || (locationManager.authorizationStatus == CLAuthorizationStatus.authorizedWhenInUse)){
            locationManager.stopUpdatingLocation()
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    @objc func restart(){
        timer?.invalidate()
        timer = nil
        
        if isItTime(now: NSDate()){
            start()
        }else{
            beginNewBackgroundTask()
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .restricted,.denied,.notDetermined:
            timer = nil
        default:
            locationManager.stopUpdatingLocation()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(timer==nil){
            // The locations array is sorted in chronologically ascending order, so the
            // last element is the most recent
            guard let location = locations.last else {return}
            beginNewBackgroundTask()
            locationManager.stopUpdatingLocation()
            
            if ((AppSettings.sharedSettings.isLocationAccessd ?? false) != true){
                sendLocationToServer(location: location)
            }else{
                sendLocationToServer(location: location)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        beginNewBackgroundTask()
        locationManager.stopUpdatingLocation()
    }

    func isItTime(now:NSDate) -> Bool {
        let timePast = now.timeIntervalSince(lastLocationDate)
        let intervalExceeded = Int(timePast) > Int(BackgroundLocationManager.BACKGROUND_TIMER)
        return intervalExceeded
    }

    func sendLocationToServer(location:CLLocation){
       let geoFirestoreRef = Firestore.firestore().collection("Geolocations")
       let geoFirestore = GeoFirestore(collectionRef: geoFirestoreRef)
        if (Auth.auth().currentUser?.uid ?? "") != ""{
          updateAvatar()
          geoFirestore.setLocation(location: location, forDocumentWithID: Auth.auth().currentUser?.uid ?? "") { (error) in
               if let error = error {
                   print("An error occured: \(error)")
               } else {
                   AppSettings.sharedSettings.isLocationAccessd = true
                   print("Saved location successfully!")
               }
           }
        }
    }
    
    //keep avatar updated
    private func updateAvatar(){
      let db = Firestore.firestore()
      SCSDKBitmojiClient.fetchAvatarURL {(avatarURL, error) in
        db.collection("users").document((Auth.auth().currentUser?.uid ?? "")).setData(["avatar":avatarURL ?? "https://sdk.bitmoji.com/render/panel/78d0f964-d545-475a-8fc2-272aeb2189ae-ATNLemdEV0UJG1uR_LuJtsYRpxYUbQ-v1.png?transparent=1&palette=1"], merge: true)
      }
    }

    func beginNewBackgroundTask(){
        var previousTaskId = currentBgTaskId;
        currentBgTaskId = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            print("task expired: ")
            if let taskId = previousTaskId{
                UIApplication.shared.endBackgroundTask(taskId)
                previousTaskId = UIBackgroundTaskIdentifier.invalid
            }
        })

        if let taskId = previousTaskId{
            if previousTaskId != UIBackgroundTaskIdentifier.invalid{
                UIApplication.shared.endBackgroundTask(taskId)
                previousTaskId = UIBackgroundTaskIdentifier.invalid
            }
        }

        timer = Timer.scheduledTimer(timeInterval: 150.0, target: self, selector: #selector(self.restart),userInfo: nil, repeats: false)
    }
}
