//
//  ViewController.swift
//  PUB
//
//  Created by Andrew Baytler on 2/13/21.
//

import UIKit
//import Mapbox
import SideMenu
import UBottomSheet
import CoreLocation
import Alamofire
import MapKit
//import MapboxCoreNavigation
//import MapboxNavigation
//import MapboxDirections
import Firebase

class ViewController: BaseController, UIGestureRecognizerDelegate {

    @IBOutlet weak var map:MKMapView!
    static var currentUser:PubUser?
    var userManager = UserManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.newCrawl),
                             name: NSNotification.Name(rawValue: "NewCrawl"),
                             object: nil)
        startLoading("")
        userManager.getUser(id: Auth.auth().currentUser?.uid ?? "") {[weak self](user) in
            if let user = user{
                DispatchQueue.main.async {
                    self?.finishLoading()
                    ViewController.currentUser = user
//                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NewCrawl"), object: nil, userInfo: nil)
                    PushNotificationManager.instance.updateFirestorePushTokenIfNeeded()
                    
                }
            }
        }
    
    }
    
    @objc func newCrawl(notification: NSNotification){
        let id = notification.userInfo?["id"] as? String
        let name = notification.userInfo?["name"] as? String
        let schduleDate = notification.userInfo?["schduleDate"] as? String

        let bars = notification.userInfo?["bars"] as! String
        let res = try! JSONDecoder().decode([Bar].self, from:Data(bars.utf8))
        let crawl = Crawl(id: id, name: name, schduleDate: schduleDate, bars:res, memebers: [], userimages: [])
        print(crawl)
    
        DispatchQueue.main.async
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CrawlNotificationVC") as! CrawlNotificationVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.crawl = crawl
            vc.callback = {[weak self] in
                self?.addSheet(crawl)
            }
            self.present(vc, animated: true, completion: nil)
          }
    }
    
    func addSheet(_ crawl:Crawl){
        let sheetCoordinator = UBottomSheetCoordinator(parent: self)
        sheetCoordinator.removeDropShadow()
        sheetCoordinator.setCornerRadius(10)
        let sheetVC = storyboard?.instantiateViewController(identifier: "CrawlSheetVC") as! CrawlSheetVC
        sheetVC.crawl = crawl
        //vc = UINavigationController(rootViewController: sheetVC)
        sheetVC.sheetCoordinator = sheetCoordinator
        sheetCoordinator.addSheet(sheetVC, to: self, didContainerCreate: { container in
            let f = self.view.frame
            let rect = CGRect(x: f.minX, y: f.minY, width: f.width, height: f.height)
            let maskPath = UIBezierPath(roundedRect: rect,
                        byRoundingCorners: [.topLeft, .topRight],
                        cornerRadii: CGSize(width: 20.0, height: 20.0))

            let shape = CAShapeLayer()
            shape.path = maskPath.cgPath
            container.layer.mask = shape
            //roundCorners(corners: [.topLeft, .topRight], radius: 10, rect: rect)
       })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func initiateBarCrawl(_ sender: UIButton) {
       
        
    }
    
    func createPolyline(mapView: MKMapView) {
        let point1 = CLLocationCoordinate2DMake(-73.761105, 41.017791);
        let point2 = CLLocationCoordinate2DMake(-73.760701, 41.019348);
        let point3 = CLLocationCoordinate2DMake(-73.757201, 41.019267);
        let point4 = CLLocationCoordinate2DMake(-73.757482, 41.016375);
        let point5 = CLLocationCoordinate2DMake(-73.761105, 41.017791);
        
        let points: [CLLocationCoordinate2D]
        points = [point1, point2, point3, point4, point5]
        
        let geodesic = MKGeodesicPolyline(coordinates: points, count: 5)
        map.addOverlay(geodesic)
        
        UIView.animate(withDuration: 1.5, animations: { () -> Void in
            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let region1 = MKCoordinateRegion(center: point1, span: span)
            self.map.setRegion(region1, animated: true)
        })
    }
    
    
    
//    func calculateRoute(from bar1: CLLocationCoordinate2D, to bar3: CLLocationCoordinate2D){
//        let bar1 = Waypoint(coordinate: bar1, coordinateAccuracy: -1, name: "Start")
//
//        let bar3 = Waypoint(coordinate: bar3, coordinateAccuracy: -1, name: "Finish")
//
//        let routeOptions = NavigationRouteOptions(waypoints: [bar1, bar3], profileIdentifier: .automobileAvoidingTraffic)
//
//        Directions.shared.calculate(routeOptions) { [weak self] (session, result) in
//            switch result {
//            case .failure(let error):
//                print(error.localizedDescription)
//            case .success(let response):
//                guard let route = response.routes?.first, let strongSelf = self else {
//                    return
//                }
//
//                strongSelf.route = route
//                strongSelf.routeOptions = routeOptions
//
//                // Draw the route on the map after creating it
//                strongSelf.drawRoute(route: route)
//
//                // Show destination waypoint on the map
//                strongSelf.mapView.showWaypoints(on: route)
//
//                // Display callout view on destination annotation
//                if let annotation = strongSelf.mapView.annotations?.first as? MGLPointAnnotation {
//                    annotation.title = "Start navigation"
//                    strongSelf.mapView.selectAnnotation(annotation, animated: true, completionHandler: nil)
//                }
//            }
//        }
    //}
    
//     func drawRoute(route: Route) {
//        guard let routeShape = route.shape, routeShape.coordinates.count > 0 else { return }
//        var routeCoordinates = routeShape.coordinates
//
//        let polyline = MGLPolylineFeature(coordinates: &routeCoordinates, count: UInt(routeCoordinates.count))
//
//        if let source = mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
//            source.shape = polyline
//        }
//        else {
//            let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
//            // Customize the route line color and width
//            let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
//            lineStyle.lineColor = NSExpression(forConstantValue: #colorLiteral(red: 0.1432347298, green: 0.5841884017, blue: 0.9614997506, alpha: 1))
//            lineStyle.lineWidth = NSExpression(forConstantValue: 4)
//
//             //Add the source and style layer of the route line to the map
//            mapView.style?.addSource(source)
//            mapView.style?.addLayer(lineStyle)
//        }
//     }
}

