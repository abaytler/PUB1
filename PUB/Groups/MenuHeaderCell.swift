//
//  MenuHeaderCell.swift
//  PUB
//
//  Created by Syed on 01/04/2021.
//

import UIKit

class MenuHeaderCell: UITableViewCell{
    @IBOutlet weak var imag:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblFriends:UILabel!
}
