
import UIKit

class FriendSelectionVC: BaseController{
    
    @IBOutlet var tableView: UITableView!
    var friends: [PubUser] = []
    var userManager = UserManager()
    var group:Group?
    var callback:((_ group:Group?)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = (modeColor == .white) ? .black : .white
        self.title = "Friends"
        tableView.reloadData()
        userManager.getUserFriends { (friends) in
            self.friends = friends.filter({!(self.group?.members?.contains($0.uid ?? "") ?? false) })
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                //self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        tableView.reloadData()
        self.view.backgroundColor = (modeColor == .white) ? .black : .white
    }
    
    @IBAction func cancelTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addFriends(_ sender:UIButton){
        let members = friends.filter({($0.isSelected ?? false) == true})
        let ids = members.map({$0.uid ?? ""})
        let names = members.map({$0.fullName})
        let pmembers = (group?.members ?? []) + ids
        let pmembersnames = (group?.membersNames ?? []) + names
        
        startLoading("")
        GroupManager.shared.addUserInGroup(group?.id ?? "", members: pmembers, membersNames: pmembersnames) {[weak self] in
            self?.finishLoading()
            DispatchQueue.main.async {
                self?.group?.members = pmembers
                self?.group?.membersNames = pmembersnames
                self?.callback?(self?.group)
                self?.dismiss(animated: true, completion: nil)
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
}

extension FriendSelectionVC: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return friends.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendSelectionCell", for: indexPath)
        let user: PubUser
        user = friends[indexPath.row]
        
        let image = (user.isSelected ?? false) ? UIImage(systemName: "checkmark.circle.fill") : UIImage(systemName: "checkmark.circle")
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.frame = CGRect(x: 20, y: 20, width: 20, height: 20)
        button.setImage(image, for: .normal)
        
        button.tag = indexPath.row
        button.addTarget(self, action:#selector(self.buttonClicked(_ :)), for: .touchUpInside)
        cell.accessoryView = button
        cell.textLabel?.text = user.fullName
        return cell
  }
    
    @objc func buttonClicked(_ sender: UIButton) {
        friends[sender.tag].isSelected = !(friends[sender.tag].isSelected ?? false)
        tableView.beginUpdates()
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
        tableView.endUpdates()
    }
}
