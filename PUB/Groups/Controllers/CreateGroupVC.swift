//
//  CreateGroupVC.swift
//  PUB
//
//  Created by Mohsin on 23/03/2021.
//

import UIKit
import SDWebImage
import Firebase

class CreateGroupVC: BaseController{
    
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var logo:UIImageView!
    @IBOutlet weak var groupField:UITextField!
    @IBOutlet weak var saveBtn:UIButton!
    @IBOutlet weak var titleTxt:UILabel!
    
    //vars
    var imagePicker = UIImagePickerController()
    var cameraPicker = UIImagePickerController()
    var imageUrl : URL!
    let fileName = "Logo_pic"
    var group:Group?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logo.makeRound()
        if let usergroup = group, let url = URL(string: usergroup.imageUrl ?? ""){
            self.logo.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.logo.sd_setImage(with: url, placeholderImage: UIImage(named: "resize-image"))
            groupField.text = usergroup.name ?? ""
            saveBtn.setTitle("Save", for: .normal)
            titleTxt.text = "Edit Group"
        }
    }
    
    //Validation
    
    private func isCheck()->Bool{
        
        guard let _ = logo.image else{
            let alertView = AlertView.prepare(title: "Alert", message: "Please select Image!", okAction: {
            })
            self.present(alertView, animated: true, completion: nil)
            return false
        }
        
        guard let name = groupField.text, name.count > 0  else {
            let alertView = AlertView.prepare(title: "Alert", message: "Please enter the Name!", okAction: {
            })
            self.present(alertView, animated: true, completion: nil)
            return false
        }

        return true
    }
    
    //Actions
    
    @IBAction func imageTapped(_ sender:UIButton){
        selectImage()
    }
    
    @IBAction func createTapped(_ sender:UIButton){
        if isCheck(){
            if let _ = group{
                updateUserGroup()
            }else{
                createUserGroup()
            }
        }
    }
    
    @IBAction func cancelTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func uploadGroupImage(_ id:String){
        guard let img = logo.image else{
            return
        }
       
        GroupManager.shared.uploadLogo(id: id, image: img, completion:
        {[weak self](response) in
            DispatchQueue.main.async {
                self?.finishLoading()
                if let _ = self?.group{
                    self?.alertMessage(message: "Group Updated Successfully", completionHandler: {
                        self?.dismiss(animated: true, completion: nil)
                    })
                }else{
                    self?.alertMessage(message: "Group Created Successfully", completionHandler: {
                        self?.dismiss(animated: true, completion: nil)
                    })
                }
                
            }
            
        })
        {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: {
                    self?.dismiss(animated: true, completion: nil)
                })
            }
        }
    }
    
    private func updateUserGroup(){
        guard var usergroup = group else {return}
        usergroup.name = groupField.text ?? ""
        
        startLoading("")
        GroupManager.shared.updateGroup(group: usergroup,
        completion:
        {[weak self](response, id) in
            DispatchQueue.main.async {
                self?.uploadGroupImage(id)
            }
        },
        failure:
        {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        })
    }
    
    private func createUserGroup(){
        let group = Group(
            createdAt: Date().timeIntervalSince1970,
            createdBy: Auth.auth().currentUser?.uid ?? "",
            id: "",
            members: [Auth.auth().currentUser?.uid ?? ""],
            membersNames: [ViewController.currentUser?.fullName ?? ""],
            name: groupField.text ?? "",
            type:1,
            imageUrl: "")
        startLoading("")
        GroupManager.shared.createGroup(group: group,
        completion:
        {[weak self](response, id) in
            DispatchQueue.main.async {
                self?.uploadGroupImage(id)
            }
        },
        failure:
        {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        })
    }
}

extension CreateGroupVC{
    
    private func selectImage(){
        let alert = UIAlertController(title: "Picture", message: nil, preferredStyle: .alert)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) {
            UIAlertAction in self.openCamera()
        }
        let libraryAction = UIAlertAction(title: "Library", style: .default) { (action) in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) {
            UIAlertAction in self.cancel()
        }
        alert.addAction(cameraAction)
        alert.addAction(libraryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            cameraPicker.sourceType = .camera
            cameraPicker.delegate = self
            cameraPicker.allowsEditing = false
            self.present(cameraPicker, animated: true, completion: nil)
        }
        else {
            let alertView = AlertView.prepare(title: "Error", message: "Camera not Available", okAction: {
            })
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    private func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            present(imagePicker, animated: true, completion: nil)
        }
        else {
            print("Not Available")
        }
    }
    
    func cancel() {
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
}

extension CreateGroupVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            logo.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
