

import UIKit
import SDWebImage
import Firebase

class GroupViewController: BaseController {
    
    @IBOutlet weak var collectionView:UICollectionView!
    
    //Vars
    let inset: CGFloat = 0
    let minimumLineSpacing: CGFloat = 1
    let minimumInteritemSpacing: CGFloat = 5
    var groups:[Group] = []
   

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = (modeColor == .white) ? .black : .white
        self.collectionView.register(UINib(nibName:"GroupCell", bundle: nil), forCellWithReuseIdentifier: "groupCell")


//        self.collectionView.register(UINib(nibName:"NewGroupCell", bundle: nil), forCellWithReuseIdentifier: "newgroupCell")
        self.fetchGroups()
        
    }
    
    private func fetchGroups(){
        //startLoading("")
        GroupManager.shared.fetchGroupByUserID(Auth.auth().currentUser?.uid ?? "") {[weak self](groups) in
            //self?.finishLoading()
            DispatchQueue.main.async {
                self?.groups = groups
                self?.collectionView.reloadData()
                if (self?.groups.isEmpty ?? false){
                    self?.alertMessage(message: "You are not part of any groups yet!", completionHandler: nil)
                }
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                //self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: {
                    self?.dismiss(animated: true, completion: nil)
                })
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        collectionView.reloadData()
        self.view.backgroundColor = (modeColor == .white) ? .black : .white
    }
    
    @IBAction func addTapped(_ sender:UIButton){
        let vc = storyboard?.instantiateViewController(identifier: "CreateGroupVC") as! CreateGroupVC
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func addFriends(_ sender:UIButton){
        let vc = storyboard?.instantiateViewController(identifier: "friendVC") as! FriendsViewController
        //vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension GroupViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
            
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.groups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "groupCell", for: indexPath as IndexPath) as! GroupCell
            cell.setup(groups[indexPath.row])
            cell.name.text = (groups[indexPath.row].name ?? "").capitalized
            cell.name.textColor = modeColor
            return cell
        }
            
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if (Auth.auth().currentUser?.uid ?? "") == (groups[indexPath.row].createdBy ?? ""){
                showAlert(groups[indexPath.row])
            }else{
                self.alertMessage(message: "You are not the creator of the group, you don't have permissions to modify this group.", completionHandler: nil)
            }
        }
}

extension GroupViewController {

    func showAlert(_ group:Group) {
        let alert = UIAlertController(title: "Please Select an Option", message: "", preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "Add Friends", style: .default , handler:{ (UIAlertAction)in
            self.performSegue(withIdentifier: "movetofriends", sender: group)
        }))
        
        
        
        alert.addAction(UIAlertAction(title: "Edit Group", style: .default , handler:{ (UIAlertAction)in
            self.performSegue(withIdentifier: "updategroup", sender: group)
        }))
        
        alert.addAction(UIAlertAction(title: "Delete Group", style: .default , handler:{ (UIAlertAction)in
            self.deleteGroup(group)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive , handler:{ (UIAlertAction)in
            self.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? CreateGroupVC{
            destinationVC.group = sender as? Group
        }else if let destination = segue.destination as? GroupUsersVC{
            destination.group = sender as? Group
        }else if let destination = segue.destination as? CreateGroupVC{
            destination.modalPresentationStyle = .overCurrentContext
        }
    }
    
    private func deleteGroup(_ group:Group){
        startLoading("")
        GroupManager.shared.deleteGroupByUserID(group.id ?? "") {[weak self](response) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: "Group Deleted Successfully", completionHandler: {
                    self?.dismiss(animated: true, completion: nil)
                })
            }
        } failure: {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: {
                    self?.dismiss(animated: true, completion: nil)
                })
            }
        }
    }
}

extension GroupViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return  minimumInteritemSpacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if groups.count > 1{
            let width = (collectionView.bounds.size.width - 8*4) / 3
            return CGSize(width: width, height: 150)
        }else{
            return CGSize(width: 100, height: 150)
        }
    }
}


class GroupCell: UICollectionViewCell{
    @IBOutlet weak var imag:UIImageView!
    @IBOutlet weak var name:UILabel!
    
    func setup(_ group:Group){
        imag.makeRound()
        if let url = URL(string: group.imageUrl ?? ""){
            self.imag.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imag.sd_setImage(with: url, placeholderImage: UIImage(named: "userImage"))
        }else{
            self.imag.image = UIImage(named: "userImage")
        }
    }
}
