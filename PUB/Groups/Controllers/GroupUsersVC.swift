
import UIKit

class GroupUsersVC: BaseController{
    
    @IBOutlet var tableView: UITableView!
    var users: [String] = []
    var ids: [String] = []
    var userManager = UserManager()
    var group:Group?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users = (group?.membersNames ?? [])
        ids = (group?.members ?? [])
        tableView.reloadData()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        tableView.reloadData()
    }
    
    @IBAction func moveToAdd(_ sender:UIBarItem){
        self.performSegue(withIdentifier: "movetoadd", sender: group)
    }
    
    @IBAction func cancelTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? FriendSelectionVC
        destination?.group = sender as? Group
        destination?.callback = {[weak self] response in
            self?.users = (response?.membersNames ?? [])
            self?.ids = (response?.members ?? [])
            self?.tableView.reloadData()
        }
    }
}

extension GroupUsersVC: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return users.count
  }
    
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath)

        let image = UIImage(named: "minusSign")
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.frame = CGRect(x: 20, y: 20, width: 20, height: 20)
        button.setImage(image, for: .normal)
        
        button.tag = indexPath.row
        button.tintColor = modeColor
        button.addTarget(self, action:#selector(self.buttonClicked(_ :)), for: .touchUpInside)
        if ids[indexPath.row]  != (ViewController.currentUser?.uid ?? ""){
            cell.accessoryView = button
        }
    cell.textLabel?.text = users[indexPath.row].capitalized
        return cell
  }
    
    @objc func buttonClicked(_ sender: UIButton) {
        users.remove(at: sender.tag)
        ids.remove(at: sender.tag)
        startLoading("")
        GroupManager.shared.deleteUserFromGroup(group?.id ?? "", members: ids, membersNames: users,completion: {[weak self] in
            
            DispatchQueue.main.async {
                
                self?.tableView.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
                self?.group?.members = self?.ids
                self?.group?.membersNames = self?.users
                self?.finishLoading()
            }
            
        }) {[weak self](error) in
            DispatchQueue.main.async {
                self?.finishLoading()
                self?.alertMessage(message: error.message, completionHandler: nil)
            }
        }
    }
}
