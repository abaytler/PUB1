

import UIKit
import CircleProgressView

class EntranceVC: UIViewController {
    
    @IBOutlet var DaysProgress: CircleProgressView!
    @IBOutlet var hoursProgress: CircleProgressView!
    @IBOutlet var minitesProgress: CircleProgressView!
    @IBOutlet var secondesProgress: CircleProgressView!
    
    @IBOutlet var valueDay: UILabel!
    @IBOutlet var valueHour: UILabel!
    @IBOutlet var valueMinites: UILabel!
    @IBOutlet var valueSeconds: UILabel!
   
    @IBOutlet var clockView: UIView!
    @IBOutlet var lblTiltle: UILabel!
   
    var timeEnd : Date?
    var crawl:Crawl!

    override func viewDidLoad() {
        super.viewDidLoad()
        //you can commented this line in order to disable the persistence
        AppSettings.sharedSettings.isCrawlJoin = true
        if let date = crawl?.getSchduleDate{
            timeEnd = date
        }
        lblTiltle.text = "You're in \(crawl?.name ?? "")"
        updateView()
    }

    func updateView() {
        setTimeLeft()
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.setTimeLeft), userInfo: nil, repeats: true)
    }
    
    @IBAction func dismiss(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func setTimeLeft() {
        let timeNow = Date()
        if timeEnd?.compare(timeNow) == ComparisonResult.orderedDescending {
            
            let interval = timeEnd?.timeIntervalSince(timeNow)
            
            let days =  (interval! / (60*60*24)).rounded(.down)
            
            let daysRemainder = interval?.truncatingRemainder(dividingBy: 60*60*24)
            
            let hours = (daysRemainder! / (60 * 60)).rounded(.down)
            
            let hoursRemainder = daysRemainder?.truncatingRemainder(dividingBy: 60 * 60).rounded(.down)
            
            let minites  = (hoursRemainder! / 60).rounded(.down)
            
            let minitesRemainder = hoursRemainder?.truncatingRemainder(dividingBy: 60).rounded(.down)
            
            let scondes = minitesRemainder?.truncatingRemainder(dividingBy: 60).rounded(.down)
            
            
            DaysProgress.setProgress(days/360, animated: false)
            hoursProgress.setProgress(hours/24, animated: false)
            minitesProgress.setProgress(minites/60, animated: false)
            secondesProgress.setProgress(scondes!/60, animated: false)
            
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 2
            
            valueDay.text = formatter.string(from: NSNumber(value:days))
            valueHour.text = formatter.string(from: NSNumber(value:hours))
            valueMinites.text = formatter.string(from: NSNumber(value:minites))
            valueSeconds.text = formatter.string(from: NSNumber(value:scondes!))
            
        } else {
            clockView.fadeOut()
        }
    }
}

extension String{
    func toDate(format : String) -> Date{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)!
    }
}

extension UIView {
    func fadeIn() {
        // Move our fade out code from earlier
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0 // Instead of a specific instance of, say, birdTypeLabel, we simply set [thisInstance] (ie, self)'s alpha
        }, completion: nil)
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }, completion: nil)
    }
}


