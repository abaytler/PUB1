const functions = require("firebase-functions");
const algoliasearch = require("algoliasearch");
const admin = require("firebase-admin");


const client = algoliasearch("G7XAW4SGV7", "c4fbf5f79b5a8c134a68398bd9211b5e");
const index = client.initIndex("users");
admin.initializeApp();

// Create a HTTP request cloud function.
// eslint-disable-next-line max-len
exports.sendCollectionToAlgolia = functions.https.onRequest(async (_req, res) => {
  const algoliaRecords = [];
  const db = admin.firestore();

  // Retrieve all documents from the COLLECTION collection.
  const querySnapshot = await db.collection("users").get();

  querySnapshot.docs.forEach((doc) => {
    const newData = doc.data();
    const objectID = doc.id;

    algoliaRecords.push({...newData, objectID});
  });

  index.saveObjects(algoliaRecords, (_error, content) => {
    res.status(200).send("COLLECTION was indexed to Algolia successfully.");
  });
});

exports.addToIndex = functions.firestore.document("users/{userId}")

    .onCreate((snapshot) => {
      const data = snapshot.data();
      const objectID = snapshot.id;

      return index.saveObject({...data, objectID});
    });

exports.updateIndex = functions.firestore.document("users/{userId}")

    .onUpdate((change) => {
      const newData = change.after.data();
      const objectID = change.after.id;
      return index.saveObject({...newData, objectID});
    });

exports.deleteFromIndex = functions.firestore.document("users/{userId}")

    .onDelete((snapshot) =>
      index.deleteObject(snapshot.id)
    );
